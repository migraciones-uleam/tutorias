export default{
  async login(user){
    const URL = 'https://tutoriasuleam.hopto.org'
    let API = [`${URL}/v1/api/user/verify`,`${URL}/v1/api/auth/signin`]
    try {
      const verifyUser = await fetch(API[0], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      })
      const res = await verifyUser.json()
      const tokenUser = await fetch(API[1], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(res)
        
      })
      const response = await tokenUser.json()
      return response
    } catch (e) {
      console.log(e)
    }
  },
  verifyRol(userRole){
    if(userRole === "profesor"){
      return { name: 'TeacherMain'}
    }if(userRole === "estudiante"){
      return { name: 'Main'}
    }else{
      return
    }
  }
}