import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/inicio',
    name: 'Main',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "main" */ '../views/Main.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/tutorias/recibidas',
    name: 'Tutoring',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "tutoring" */ '../views/Tutoring.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/tutorias/enviadas',
    name: 'Sended',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/Sended.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/usuario',
    name: 'ProfileStudent',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/ProfileStudent.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/p/usuario',
    name: 'ProfileTeacher',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/ProfileTeacher.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/tutorias/:id',
    name: 'SendTutoring',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/SendTutoring.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/p/inicio',
    name: 'TeacherMain',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/TeacherMain.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/p/tutorias/enviadas',
    name: 'TeacherSended',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/TeacherSended.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/p/tutorias/recibidas',
    name: 'TeacherTutoring',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/TeacherTutoring.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/p/tutorias/:id',
    name: 'TeacherSendTutoring',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "sended" */ '../views/TeacherSendTutoring.vue'),
    meta: { requireAuth: true }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next)=>{

  const isProtected = to.matched.some(item=>item.meta.requireAuth)
  const auth = store.getters.getToken

  if(isProtected && auth === null){
    next('/')
  }else{
    next()
  }
})

export default router
