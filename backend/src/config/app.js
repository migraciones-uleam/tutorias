// Core de NodeJS
const express = require('express')

// Terceros
const cors = require('cors')
const morgan = require('morgan')
const helmet = require('helmet')
const compression = require('compression')

// Opciones de cors
let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

// RUTAS
const {
  userRoutes,
  subjectRoutes,
  tutoringRoutes,
  authRoutes
} = require('../routes')

// APP
const app = express()

// Nivel 1
const router = express.Router()

// Nivel 2
const apiRoutes = express.Router()

apiRoutes
  .use(express.json())
  .use(cors(corsOptions))
  .use(helmet())
  .use(compression())
  .use(morgan('dev'))

apiRoutes.use('/user', userRoutes)
apiRoutes.use('/subject', subjectRoutes)
apiRoutes.use('/tutoring', tutoringRoutes)
apiRoutes.use('/auth', authRoutes)

router.use('/v1/api', apiRoutes)

app.use(router)

module.exports = app