const { User } = require('../models')

const user = {}

// Obtener usuarios
user.get = async(req, res)=>{
  try {
    const users = await User.find()
    res.json(users)
  } catch (e) {
    console.log(e)
  }
}

// Obtener por Id
user.getById = async(req, res)=>{
  const { userId } = req.params
  try {
    const user = await User.findById(userId)
    res.json(user)
  } catch (e) {
    console.log(e)
  }
}

// Create
user.create = async(req, res)=>{
  const { body } = req
  try {
    const createdUser = await User.create(body)
    res.json(createdUser)
  } catch (e) {
    console.log(e)
  }
}

// Update
user.update = async(req, res)=>{
  const { body } = req
  const { userId } = req.params
  try {
    const updateUser = await User.findByIdAndUpdate(userId, body, {new:true})
    res.json(updateUser)
  } catch (e) {
    console.log(e)
  }
}

// Delete
user.delete = async(req, res)=>{
  const { userId } = req.params
  try {
    const deletedUser = await User.findByIdAndDelete(userId)
    res.json(deletedUser)
  } catch (e) {
    console.log(e)
  }
}

module.exports = user