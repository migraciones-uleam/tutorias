const { Tutoring, Subject } = require('../models')

const tutoring = {}

// Obtener tutorias
tutoring.get = async(req, res)=>{
  try {
    const tutorings = await Tutoring.find()
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Obtener por Id
tutoring.getById = async(req, res)=>{
  const { tutoringId } = req.params
  try {
    const tutorship = await Tutoring.findById(tutoringId)
    res.json(tutorship)
  } catch (e) {
    console.log(e)
  }
}

// Create
tutoring.create = async(req, res)=>{
  const { body } = req
  req.body.autor = req.userId
  req.body.date = `${req.body.date}Z`
  try {
    const createdTutoring = await Tutoring.create(body)
    res.json(createdTutoring)
  } catch (e) {
    console.log(e)
  }
}

// Update
tutoring.update = async(req, res)=>{
  const { body } = req
  const { tutoringId } = req.params
  try {
    const updateTutoring = await Tutoring.findByIdAndUpdate(tutoringId, body, {new:true})
    res.json(updateTutoring)
  } catch (e) {
    console.log(e)
  }
}

// Delete
tutoring.delete = async(req, res)=>{
  const { tutoringId } = req.params
  try {
    const deletedTutoring = await Tutoring.findByIdAndDelete(tutoringId)
    res.json(deletedTutoring)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias enviadas... pendientes
tutoring.getPending = async(req, res)=>{
  const query = { autor: {_id: req.userId}, state: 'Pendiente' }
  try {
    const tutorings = await Tutoring.find(query)
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias enviadas... aceptadas
tutoring.getAcepted = async(req, res)=>{
  const query = { autor: {_id: req.userId}, state: 'Aceptada' }
  try {
    const tutorings = await Tutoring.find(query)
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias enviadas... rechazadas
tutoring.getCanceled = async(req, res)=>{
  const query = { autor: {_id: req.userId}, state: 'Rechazada' }
  try {
    const tutorings = await Tutoring.find(query)
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... pendientes profesor
tutoring.getPendingTeacher = async(req, res)=>{
  // const query = {
  //   autor: {_id: {$ne: req.userId} }, state: 'Pendiente',
  //   subject: {teacher: { _id: req.userId}} 
  // }
  let arr = []
  try {
    const
      query = { teacher: {_id: req.userId}},
      proyection = { teacher: 0, students: 0, name: 0, class:0},
      subjects = await Subject.find(query, proyection)
      subjects.forEach(el=>arr.push(el._id))
    
    const
      tutorings = await Tutoring.find()
      .where('autor').ne(req.userId)
      .where('state', 'Pendiente')
      .where('subject').in(arr)
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... aceptadas profesor
tutoring.getAceptedTeacher = async(req, res)=>{
  let arr = []
  try {
    const
      query = { teacher: {_id: req.userId}},
      proyection = { teacher: 0, students: 0, name: 0, class:0},
      subjects = await Subject.find(query, proyection)
      subjects.forEach(el=>arr.push(el._id))
    
    const
      tutorings = await Tutoring.find()
      .where('autor').ne(req.userId)
      .where('state', 'Aceptada')
      .where('subject').in(arr)
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... rechazadas profesor
tutoring.getCanceledTeacher = async(req, res)=>{
  let arr = []
  try {
    const
      query = { teacher: {_id: req.userId}},
      proyection = { teacher: 0, students: 0, name: 0, class:0},
      subjects = await Subject.find(query, proyection)
      subjects.forEach(el=>arr.push(el._id))
    
    const
      tutorings = await Tutoring.find()
      .where('autor').ne(req.userId)
      .where('state', 'Rechazada')
      .where('subject').in(arr)
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... pendientes estudiante
tutoring.getPendingStudent = async(req, res)=>{
  try {
    const query = { assistants: {_id: req.userId}}
    
    const
      tutorings = await Tutoring.find(query)
      .where('autor').ne(req.userId)
      .where('state', 'Pendiente')
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... aceptadas estudiante
tutoring.getAceptedStudent = async(req, res)=>{
  try {
    const query = { assistants: {_id: req.userId}}
    
    const
      tutorings = await Tutoring.find(query)
      .where('autor').ne(req.userId)
      .where('state', 'Aceptada')
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

// Tutorias recibidas... rechazadas estudiante
tutoring.getCanceledStudent = async(req, res)=>{
  try {
    const query = { assistants: {_id: req.userId}}
    
    const
      tutorings = await Tutoring.find(query)
      .where('autor').ne(req.userId)
      .where('state', 'Rechazada')
    
    res.json(tutorings)
  } catch (e) {
    console.log(e)
  }
}

module.exports = tutoring