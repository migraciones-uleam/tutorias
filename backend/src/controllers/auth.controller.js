const jwt = require('jsonwebtoken')
const { SECRET } = require('../config')
const { User } = require('../models')

const auth = {}

auth.singIn = async(req, res)=>{

  const { email, password } = req.body

  try {
    // verificar email
    const user = await User.findOne({email})
    if(!user){
      return res
        .status(400)
        .json({message: 'Usuario no encontrado'})
    }

    // verificar contraseña
    const validPassword = await User.comparePassword(
      password, user.password
    )
    if(!validPassword){
      return res
        .status(400)
        .json({message: 'Contraseña incorrecta'})
    }

    // generar token
    const token = jwt.sign({id: user._id}, SECRET, {
      expiresIn: 86400
    })

    res.json({user, token})
  } catch (e) {
    res
      .status(500)
      .json({message: 'Hable con un administrador'})
  }
}

module.exports = auth