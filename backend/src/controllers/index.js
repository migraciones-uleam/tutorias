module.exports = {
  userController: require('./user.controller'),
  subjectController: require('./subject.controller'),
  tutoringController: require('./tutoring.controller'),
  authController: require('./auth.controller')
}