const { Subject } = require('../models')
const { validateTeacher } = require('../helpers')

const subject = {}

// Obtener materias
subject.get = async(req, res)=>{
  try {
    const subjects = await Subject.find()
    res.json(subjects)
  } catch (e) {
    console.log(e)
  }
}

// Obtener por Id
subject.getById = async(req, res)=>{
  const { subjectId } = req.params
  try {
    const grade = await Subject.findById(subjectId)
    res.json(grade)
  } catch (e) {
    console.log(e)
  }
}

// Create
subject.create = async(req, res)=>{
  const { body } = req
  try {

    // validar id del profesor
    const validateId = await validateTeacher(body.teacher)
    body.teacher = validateId

    const createdSubject = await Subject.create(body)
    res.json(createdSubject)
  } catch (e) {
    res.json(e)
  }
}

// Update
subject.update = async(req, res)=>{
  const { body } = req
  const { subjectId } = req.params
  try {
    const updateSubject = await Subject.findByIdAndUpdate(subjectId, body, {new:true})
    res.json(updateSubject)
  } catch (e) {
    console.log(e)
  }
}

// Delete
subject.delete = async(req, res)=>{
  const { subjectId } = req.params
  try {
    const deletedSubject = await Subject.findByIdAndDelete(subjectId)
    res.json(deletedSubject)
  } catch (e) {
    console.log(e)
  }
}

// Materias de un estudiante
subject.getSubjects = async(req, res)=>{
  const query = { students: {_id: req.userId}}
  const proyection = { students: 0 }
  try {
    const subjects = await Subject.find(query, proyection)
    res.json(subjects)
  } catch (e) {
    console.log(e)
  }
}

// Materias de un profesor
subject.getSubjectsTeacher = async(req, res)=>{
  const query = { teacher: {_id: req.userId}}
  const proyection = { students: 0 }
  try {
    const subjects = await Subject.find(query, proyection)
    res.json(subjects)
  } catch (e) {
    console.log(e)
  }
}

module.exports = subject