module.exports = {
  User: require('./user.model'),
  Subject: require('./subject.model'),
  Tutoring: require('./tutoring.model')
}