const mongoose = require('mongoose')
const { Schema } = mongoose

const TutoringSchema = new Schema({
  autor: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
    autopopulate: {
      select: "name lastname email"
    }
  },
  date: {
    type: Date,
    require: true
  },
  topic: {
    type: String,
    require: true
  },
  description: {
    type: String,
    require: true
  },
  state: {
    type: String,
    require: false,
    default: "Pendiente",
    enum: {
      values: ['Aceptada', 'Rechazada', 'Pendiente'],
      message: '{VALUE} no es estado de tutoría válido'
    }
  },
  subject: {
    type: Schema.Types.ObjectId,
    ref: 'subject',
    required: true,
    autopopulate: {
      select: "name class teacher"
    }
  },
  comunication: {
    type: String,
    require: true
  },
  software: {
    type: String,
    require: true
  },
  type: {
    type: String,
    require: true
  },
  assistants: [
    {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true,
      autopopulate: true
    }
  ]
},{
  versionKey: false
})

TutoringSchema.plugin(require('mongoose-autopopulate'))
module.exports = mongoose.model('tutoring', TutoringSchema)