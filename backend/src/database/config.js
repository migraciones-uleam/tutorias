const mongoose = require('mongoose')
const { MONGO_URI } = require('../config')

// Conexión a la base de datos
const conexion = () => {
  mongoose.connect(MONGO_URI, {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false
  })
    .then(()=>{
      console.log('Conexión correcta a la base de datos')
    })
    .catch(err =>{
      console.error(err)
    })
}

module.exports = conexion