const jwt = require('jsonwebtoken')
const { SECRET } = require('../config')
const { User } = require('../models')

module.exports = verifyToken = async(req, res, next)=>{
  try {
    // verificar token
    const token = req.headers['x-access-token']
    if (!token){
      return res
        .status(403)
        .json({message: 'No hay token en la petición'})
    }

    // verificar si existe el usuario
    const decoded = jwt.verify(token, SECRET)
    req.userId = decoded.id

    const user = await User.findById(req.userId)
    if(!user){
      return res
        .status(404)
        .json({message: 'El usuario no fue encontrado'})
    }
    next()
  } catch (e) {
    return res
      .status(401)
      .json({message: 'No tiene autorización'})
  }
}