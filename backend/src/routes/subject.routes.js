const { Router } = require('express')
const { subjectController } = require('../controllers')
const { verifyToken } = require('../middlewares')
const router = Router()

router.get('/', subjectController.get)

router.get('/teacher', verifyToken ,subjectController.getSubjectsTeacher)

router.post('/', subjectController.create)

router.get('/all', verifyToken ,subjectController.getSubjects)

router.get('/:subjectId', subjectController.getById)

router.put('/:subjectId', subjectController.update)

router.delete('/:subjectId', subjectController.delete)

module.exports = router