const { Router } = require('express')
const { tutoringController } = require('../controllers')
const { verifyToken } = require('../middlewares')
const router = Router()

router.get('/', tutoringController.get)

router.get('/e/received/canceled', verifyToken, tutoringController.getCanceledStudent)

router.get('/e/received/acepted', verifyToken, tutoringController.getAceptedStudent)

router.get('/e/received/pending', verifyToken, tutoringController.getPendingStudent)

router.get('/t/received/canceled', verifyToken, tutoringController.getCanceledTeacher)

router.get('/t/received/acepted', verifyToken, tutoringController.getAceptedTeacher)

router.get('/t/received/pending', verifyToken, tutoringController.getPendingTeacher)

router.get('/acepted', verifyToken, tutoringController.getAcepted)

router.get('/canceled', verifyToken, tutoringController.getCanceled)

router.get('/pending', verifyToken, tutoringController.getPending)

router.post('/', verifyToken, tutoringController.create)

router.get('/:tutoringId', tutoringController.getById)

router.put('/:tutoringId', verifyToken, tutoringController.update)

router.delete('/:tutoringId', tutoringController.delete)

module.exports = router