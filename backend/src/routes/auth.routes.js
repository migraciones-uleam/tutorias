const { Router } = require('express')
const { authController } = require('../controllers')

const router = Router()

router.post('/signin', authController.singIn)

module.exports = router