module.exports = {
  userRoutes: require('./user.routes'),
  subjectRoutes: require('./subject.routes'),
  tutoringRoutes: require('./tutoring.routes'),
  authRoutes: require('./auth.routes')
}